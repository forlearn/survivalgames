package pl.mc4e.ery65.survivalgames.configuration;

import java.io.File;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import pl.mc4e.ery65.survivalgames.SurvivalGames;

public class KitsConfig {
    
    private static FileConfiguration cfg;
    
    static {
        File file = new File(SurvivalGames.getInstance().getDataFolder() + File.separator + "kits.yml");
        if (!file.exists()){
            SurvivalGames.getInstance().getDataFolder().mkdirs();
            SurvivalGames.copy(SurvivalGames.getInstance().getResource("Resources/kits.default"), file);
        }
        cfg = YamlConfiguration.loadConfiguration(file);
    }
    
    public static ConfigurationSection getSection(String path){
        return cfg.getConfigurationSection(path);
    }
    
}
