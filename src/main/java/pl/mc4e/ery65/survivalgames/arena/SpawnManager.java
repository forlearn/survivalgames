package pl.mc4e.ery65.survivalgames.arena;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;

public class SpawnManager {
    
    private Set<Location> spawns = new HashSet<Location>();
    private Location[] loc;
    
    public SpawnManager(){}
    
    public void addSpawnLocation(Location spawn){
        spawns.add(spawn);
    }
    
    public void addSpawnsLocation(List<Location> spawn){
        spawns.addAll(spawn);
    }
    
    public boolean contains(List<Location> loc){
        return spawns.containsAll(loc);
    }
    
    public boolean contains(Location loc){
        return spawns.contains(loc);
    }
    
    public boolean contains(int index){
        return spawns.size() < index;
    }
    
    public Location getSpawnLocation(int index){
        if (!contains(index)){
            throw new ArrayIndexOutOfBoundsException();
        }
        return spawns.toArray(loc)[index];
    }
    
}
