package pl.mc4e.ery65.survivalgames.managers;

import java.util.Map;

import org.bukkit.entity.Player;

import pl.mc4e.ery65.survivalgames.api.SGPlayer;

import com.google.common.collect.Maps;

public class PlayerManager {
    
    private Map<String, SGPlayer> players = Maps.newHashMap();

    public void addPlayer(SGPlayer player) {
        this.players.put(player.getName().toLowerCase(), player);
    }

    public void removePlayer(String name) {
        if (this.players.containsKey(name.toLowerCase())) {
            this.players.get(name.toLowerCase()).save();
            this.players.remove(name.toLowerCase());
            if ((this.players == null) || (this.players.isEmpty()))
                this.players = Maps.newHashMap();
        }
    }

    public void removePlayer(SGPlayer player) {
        removePlayer(player.getName());
    }
    
    public void removePlayer(Player player){
        removePlayer(player.getName());
    }
    
    public SGPlayer getPlayer(String name){
        return players.get(name.toLowerCase());
    }
    
    public SGPlayer getPlayer(Player player){
        return getPlayer(player.getName());
    }
    
    public boolean contains(String name){
        return players.containsKey(name.toLowerCase());
    }
    
    public boolean contains(Player player){
        return contains(player.getName());
    }
    
    public void saveAll(){
        for (Map.Entry<String, SGPlayer> pl : players.entrySet()){
            pl.getValue().save();
        }
    }
    
    public void saveAllShutdown(){
    	for (Map.Entry<String, SGPlayer> pl : players.entrySet()){
            pl.getValue().save();
            players.remove(pl.getKey());
        }
    }
    
}