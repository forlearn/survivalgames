package pl.mc4e.ery65.survivalgames.utils;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import pl.mc4e.ery65.survivalgames.SurvivalGames;
import pl.mc4e.ery65.survivalgames.configuration.PluginConfig;

public class Connector {
    
    public static void sendPlayerToServer(Player player, String serverName, CommandSender cs){
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        DataOutputStream d = new DataOutputStream(out);
        try {
            d.writeUTF("Connect");
            d.writeUTF(serverName);
        } catch (IOException ignored){
            cs.sendMessage("§cFailed to send player to server §6" + serverName + "§c!");
            return;
        }
        player.sendPluginMessage(SurvivalGames.getInstance(), "BungeeCord", out.toByteArray());
    }
    
    public static void connectLobby(Player player){
        if (PluginConfig.LOBBY == null){
            SurvivalGames.getInstance().getServer().getConsoleSender().sendMessage("§cLobby name can\'t be null!");
            return;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        DataOutputStream d = new DataOutputStream(out);
        try {
            d.writeUTF("Connect");
            d.writeUTF(PluginConfig.LOBBY);
        } catch (IOException ignored){
            return;
        }
        player.sendPluginMessage(SurvivalGames.getInstance(), "BungeeCord", out.toByteArray());
    }
    
    public static void connectLobbyAll(){
    	if (PluginConfig.LOBBY == null){
            SurvivalGames.getInstance().getServer().getConsoleSender().sendMessage("§cLobby name can\'t be null!");
            return;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        DataOutputStream d = new DataOutputStream(out);
        try {
            d.writeUTF("Connect");
            d.writeUTF(PluginConfig.LOBBY);
        } catch (IOException ignored){
            return;
        }
        for (Player p : Bukkit.getServer().getOnlinePlayers()){
        	p.sendPluginMessage(SurvivalGames.getInstance(), "BungeeCord", out.toByteArray());
        }
    }
    
}
