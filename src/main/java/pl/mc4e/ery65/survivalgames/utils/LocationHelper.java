package pl.mc4e.ery65.survivalgames.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class LocationHelper {
    
    public static String parseToString(Location loc){
        StringBuilder builder = new StringBuilder();
        builder.append(loc.getWorld().getName())
        .append(",")
        .append(loc.getX())
        .append(",")
        .append(loc.getY())
        .append(",")
        .append(loc.getZ())
        .append(",")
        .append(loc.getYaw())
        .append(",")
        .append(loc.getPitch());
        return builder.toString();
    }
    
    public static Location parseFromString(String location){
        String[] loc = location.split(",");
        if(loc.length == 6){
            return new Location(Bukkit.getWorld(loc[0]),
                    Double.parseDouble(loc[1]), Double.parseDouble(loc[2]), Double.parseDouble(loc[3])
                    , Float.parseFloat(loc[4]), Float.parseFloat(loc[5]));
        } else
            throw new IllegalStateException("You entered wrong Location!");
    }

}
