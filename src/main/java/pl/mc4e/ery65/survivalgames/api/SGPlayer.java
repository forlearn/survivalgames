package pl.mc4e.ery65.survivalgames.api;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pl.mc4e.ery65.survivalgames.SurvivalGames;

public class SGPlayer {
    
    private String name;
    
    private int kills = 0;
    private int deaths = 0;
    private int tokens = 0;
    private int games_win = 0;
    private int games_lose = 0;
    
    public SGPlayer(String name){
        this.name = name;
        if (exists())
            load();
    }
    
    public SGPlayer(final SGPlayer other){
        name = other.name;
        kills = other.kills;
        deaths = other.deaths;
        tokens = other.tokens;
        games_win = other.games_win;
        games_lose = other.games_lose;
    }
    
    public SGPlayer(String name, int kills, int deaths, int tokens, int games_win, int games_lose){
        this.name = name;
        this.kills = kills;
        this.deaths = deaths;
        this.tokens = tokens;
        this.games_win = games_win;
        this.games_lose = games_lose;
    }
    
    public void load(){
        SurvivalGames.getMySQL().refresh();
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = SurvivalGames.getMySQL().getConnection()
                    .prepareStatement("SELECT * FROM `" + SurvivalGames.getMySQL().getName() + "` WHERE `name`=?");
            st.setString(1, name);
            rs = st.executeQuery();
            if (rs.next()){
                kills = rs.getInt("kills");
                deaths = rs.getInt("deaths");
                tokens = rs.getInt("tokens");
                games_win = rs.getInt("games_win");
                games_lose = rs.getInt("games_lose");
            }
        } catch (SQLException e) {
            //ignored
        } finally {
            SurvivalGames.getMySQL().closeResources(st, rs);
        }
        
    }
    
    public boolean exists(){
        SurvivalGames.getMySQL().refresh();
        return SurvivalGames.getMySQL().contains(name);
    }
    
    public void save(){
        if (name == null)
            return;
        if (deaths == 0 && kills == 0 && tokens == 0 && games_win == 0 && games_lose == 0){
            return;
        }
        SurvivalGames.getMySQL().refresh();
        PreparedStatement st = null;
        try {
            st = SurvivalGames.getMySQL().getConnection().prepareStatement("INSERT INTO `" 
            		+ SurvivalGames.getMySQL().getName() + "`"
                    + " (`name`, `games_win`, `games_lose`, `tokens`, `kills`, `deaths`) VALUES (?,?,?,?,?,?)"
                    + " ON DUPLICATE KEY UPDATE `name`=VALUES(`name`), `games_win`=VALUES(`games_win`),"
                    + " `games_lose`=VALUES(`games_lose`), `tokens`=VALUES(`tokens`),"
                    + " `kills`=VALUES(`kills`), `deaths`=VALUES(`deaths`)");
            st.setString(1, name);
            st.setInt(2, games_win);
            st.setInt(3, games_lose);
            st.setInt(4, tokens);
            st.setInt(5, kills);
            st.setInt(6, deaths);
            st.executeUpdate();
        } catch (SQLException ignored){
            ignored.printStackTrace();
            //ignored
        } finally {
            SurvivalGames.getMySQL().closeResources(st, null);
        }
    }
    
    public String getName(){
        return name;
    }
    
    @Override
    public boolean equals(Object obj){
        if (obj == null)
            return false;
        if (!getClass().equals(obj.getClass()))
            return false;
        if (obj == this)
            return true;
        final SGPlayer other = (SGPlayer) obj;
        return this.name.equals(other.name);
    }
    
    @Override
    public int hashCode(){
        int hash = 7;
        hash = 89 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }
    
    @Override
    public String toString(){
        return getClass().getSimpleName() + "[Kills:" + kills + ", Deaths:" + deaths + 
                ", Tokens:" + tokens + ", WinGames:" + games_win + ", LostGames:" + games_lose + "]";
    }
    
}
