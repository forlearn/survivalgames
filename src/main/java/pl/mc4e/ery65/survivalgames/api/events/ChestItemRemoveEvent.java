package pl.mc4e.ery65.survivalgames.api.events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.InventoryHolder;

public class ChestItemRemoveEvent extends ChestEvent {
    
    private static final HandlerList handlers = new HandlerList();

    public ChestItemRemoveEvent(Player player, InventoryHolder holder) {
        super(player, holder);
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList(){
        return handlers;
    }
    
}
