package pl.mc4e.ery65.survivalgames.api.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.inventory.InventoryHolder;

public abstract class ChestEvent extends Event {
    
    
    
    protected Player player;
    protected InventoryHolder chest;
    
    protected ChestEvent(Player player, InventoryHolder holder){
        this.player = player;
        chest = holder;
    }
    
}
